﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskProject.Migrations
{
    public partial class MyFirstMigrationTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Assistants",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    DisabledId = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    Gander = table.Column<int>(nullable: false),
                    BornDate = table.Column<string>(nullable: true),
                    MotherName = table.Column<string>(nullable: true),
                    NearingId = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assistants", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Disableds",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    DisabledId = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    BornDate = table.Column<string>(nullable: true),
                    Gander = table.Column<int>(nullable: false),
                    MotherName = table.Column<string>(nullable: true),
                    IsDone = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Disableds", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Assistants");

            migrationBuilder.DropTable(
                name: "Disableds");
        }
    }
}
