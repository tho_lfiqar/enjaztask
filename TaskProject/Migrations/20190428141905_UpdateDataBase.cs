﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskProject.Migrations
{
    public partial class UpdateDataBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "id",
                table: "Disableds",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Assistants",
                newName: "Id");

            migrationBuilder.AddColumn<int>(
                name: "GovId",
                table: "Disableds",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GovId",
                table: "Disableds");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Disableds",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Assistants",
                newName: "id");
        }
    }
}
