﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Models;
using TaskProject.Models.Dto;

namespace TaskProject.Controllers
{
    [Authorize(Roles = "User, Admin")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DisabledController : Controller
    {
        private readonly MyAppContext _context;

        public DisabledController(MyAppContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetDisabledById(string disabledId)
        {
            if (!string.IsNullOrEmpty(disabledId))
            {
                var disabledData = _context.Disableds.FirstOrDefault(c => c.DisabledId == disabledId);
                var assistantData = _context.Assistants.FirstOrDefault(c => c.DisabledId == disabledId);
                var data = new DisabledInformationDto
                {
                    FullName = disabledData.FullName,
                    BornDate = disabledData.BornDate,
                    Gander = disabledData.Gander,
                    MotherName = disabledData.MotherName,
                    IsDone = disabledData.IsDone,
                    GovId = disabledData.GovId,
                    AssistantFullName = assistantData.FullName,
                    AssistantBornDate = assistantData.BornDate,
                    AssistantGander = assistantData.Gander,
                    AssistantMotherName = assistantData.MotherName,
                    AssistantNearingId = assistantData.NearingId,
                    AssistantPhoneNumber = assistantData.PhoneNumber
                };
                return Ok(data);
            }
            return BadRequest("Error");
        }
        [HttpGet]
        public IActionResult GetDisabledByName(string fullName)
        {
            if (!string.IsNullOrEmpty(fullName))
            {
                var disabledData = _context.Disableds.FirstOrDefault(c => c.FullName == fullName);
                var assistantData = _context.Assistants.FirstOrDefault(c => c.DisabledId == disabledData.DisabledId);
                var data = new DisabledInformationDto
                {
                    FullName = disabledData.FullName,
                    BornDate = disabledData.BornDate,
                    Gander = disabledData.Gander,
                    MotherName = disabledData.MotherName,
                    IsDone = disabledData.IsDone,
                    GovId = disabledData.GovId,
                    AssistantFullName = assistantData.FullName,
                    AssistantBornDate = assistantData.BornDate,
                    AssistantGander = assistantData.Gander,
                    AssistantMotherName = assistantData.MotherName,
                    AssistantNearingId = assistantData.NearingId,
                    AssistantPhoneNumber = assistantData.PhoneNumber
                };
                return Ok(data);
            }
            return BadRequest("Error");
        }
        [HttpPost]
        public IActionResult SetIsDone([FromForm]SetIsDoneDto setIsDone)
        {
            if (!string.IsNullOrEmpty(setIsDone.DisabledId))
            {
                var disabledData = _context.Disableds.FirstOrDefault(c => c.DisabledId == setIsDone.DisabledId);
                if (disabledData != null)
                {
                    disabledData.IsDone = true;
                    _context.Disableds.Update(disabledData);
                    _context.SaveChanges();
                    var commet = new Commet
                    {
                        DisabledId = setIsDone.DisabledId,
                        CommetId = setIsDone.CommetId,
                        UserId = disabledData.Id,
                        //OredrNumber = 1,
                        RegisterDate = DateTime.Now
                    };
                    _context.Add(commet);
                    _context.SaveChanges();
                    var data = new PrintOrderDto
                    {
                        DisabledName = disabledData.FullName,
                        OrderDate = commet.RegisterDate,
                        OrderNumber = commet.OredrNumber,
                        CommetId = commet.CommetId
                    };
                    return Ok(data);
                }
            }
            return BadRequest();
        }
        [HttpGet]
        public IActionResult SearchBetweenDates(DateTime startDate, DateTime endDate)
        {
            if(startDate != null && endDate != null)
            {
                var commetData = _context.Commets.Where(c => c.RegisterDate >= startDate && c.RegisterDate <= endDate).ToList();
                var data = new List<SearchBetweenDatesDto>();
                foreach (var item in commetData)
                {
                    data.Add(new SearchBetweenDatesDto
                    {
                        
                        FullName = _context.Disableds.FirstOrDefault(c => c.DisabledId == item.DisabledId).FullName,
                        DisabledId = item.DisabledId,
                        RegisterDate = item.RegisterDate
                    });
                }
                return Ok(data);
            }
            return BadRequest();
        }
        
    }
}
