﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using TaskProject.Models;
using TaskProject.Models.Dto;

namespace TaskProject.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly MyAppContext _context;

        public AuthController(MyAppContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult AddUser([FromForm]UserDto user)
        {
            var data = new User
            {
                UserName = user.UserName,
                Password = user.Password,
                ExpairDate = user.ExpairDate,
                Name = user.Name,
                Role = user.Role
            };
            _context.Users.Add(data);
            _context.SaveChanges();
            return Ok(data);
        }
        [HttpPost]
        public IActionResult Login([FromForm]LoginDto login)
        {
            var loginData = _context.Users.FirstOrDefault(u => u.UserName == login.UserName && u.Password == login.Password);
            if (loginData == null)
            {
                return BadRequest();
            }
            else
            {
                var userName = loginData.UserName;
                if (userName == null) userName = "";
                var name = loginData.Name;
                if (name == null) name = "";
                var role = loginData.Role;
                if (role == null) role = "";

                var claims = new[]
                {
                new Claim(JwtRegisteredClaimNames.Sub, loginData.Id.ToString()),
                new Claim("UserName", userName),
                new Claim(ClaimTypes.Name, name),
                new Claim(ClaimTypes.Role, role),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

                var token = new JwtSecurityToken
                (
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(3),
                    notBefore: DateTime.UtcNow,
                    audience: "Audience",
                    issuer: "Issuer",
                    signingCredentials: new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes("wwBI_HtXqI3UgQHQ_rDRnSQRxFL1SR8fbQoS-Hsau1")),
                        SecurityAlgorithms.HmacSha256)
                );
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });
            }
        }
    }
}
