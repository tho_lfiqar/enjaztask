﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Models
{
    public class Disabled
    {

        [Key] public Guid Id { get; set; }

        public string DisabledId { get; set; }
        public string FullName { get; set; }
        public string BornDate { get; set; }
        public int Gander { get; set; }
        public string MotherName { get; set; }
        public bool IsDone { get; set; }
        public int GovId { get; set; }
    }
}
