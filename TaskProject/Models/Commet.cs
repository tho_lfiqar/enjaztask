﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Models
{
    public class Commet
    {
        [Key] public Guid Id { get; set; }
        public string DisabledId { get; set; }
        public int CommetId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OredrNumber { get; set; }
        public Guid UserId { get; set; }
        public DateTime RegisterDate { get; set; } = DateTime.Now;
    }
}
