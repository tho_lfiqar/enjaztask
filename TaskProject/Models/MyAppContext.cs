﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TaskProject.Models
{
    public class MyAppContext : DbContext
    {
        public DbSet<Disabled> Disableds { get; set; }
        public DbSet<Assistant> Assistants { get; set; }
        public DbSet<Commet> Commets { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(
                "Server=127.0.0.1;Port=5000;Database=Disabled;User Id=postgres;Password=root");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ForNpgsqlUseIdentityColumns();
            modelBuilder.Entity<User>()
            .HasIndex(p => p.UserName);
        }
    }
    
}
