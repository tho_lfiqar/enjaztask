﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Models
{
    public class Assistant
    {
        [Key] public Guid Id { get; set; }

        public string DisabledId { get; set; }
        public string FullName { get; set; }
        public int Gander { get; set; }
        public string BornDate { get; set; }
        public string MotherName { get; set; }
        public string NearingId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
