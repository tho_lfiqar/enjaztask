﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Models.Dto
{
    public class UserDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ExpairDate { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
    }
}
