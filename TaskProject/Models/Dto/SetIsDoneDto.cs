﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Models.Dto
{
    public class SetIsDoneDto
    {
        public string DisabledId { get; set; }
        public int CommetId { get; set; }
    }
}
