﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Models.Dto
{
    public class PrintOrderDto
    {
        public int OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public string DisabledName { get; set; }
        public int CommetId { get; set; }
    }
}
