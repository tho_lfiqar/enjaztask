﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Models.Dto
{
    public class SearchBetweenDatesDto
    {
        public string DisabledId { get; set; }
        public string FullName { get; set; }
        public DateTime RegisterDate { get; set; }
    }
}
