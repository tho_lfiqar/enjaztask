﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Models.Dto
{
    public class DisabledInformationDto
    {
        public string DisabledId { get; set; }
        public string FullName { get; set; }
        public string BornDate { get; set; }
        public int Gander { get; set; }
        public string MotherName { get; set; }
        public bool IsDone { get; set; }
        public int GovId { get; set; }
        public string AssistantFullName { get; set; }
        public int AssistantGander { get; set; }
        public string AssistantBornDate { get; set; }
        public string AssistantMotherName { get; set; }
        public string AssistantNearingId { get; set; }
        public string AssistantPhoneNumber { get; set; }
    }
}
