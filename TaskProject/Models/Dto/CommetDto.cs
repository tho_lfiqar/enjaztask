﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Models.Dto
{
    public class CommetDto
    {
        public string DisabledId { get; set; }
        public int CommetId { get; set; }
        public string CommetDate { get; set; }
        public int OredrNumber { get; set; }
        public Guid UserId { get; set; }
        public string RegisterDate { get; set; } = DateTime.Now.ToString();
    }
}
